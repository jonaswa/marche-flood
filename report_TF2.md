# Marche Flood September 2022
## Report of the second TaskForce field trip 01-05 May 2023 - NatRiskChange
### Paul Voit, Jonas Wassmer

![](results/figs/destroyedhouse_small.jpeg)    
Figure 1: Destroyed house below the city walls of Pergola.*

   

![](results/figs/hazardmap.png)   
*Figure 2: Most affected catchments in the Marche region and the route that we drove (dashed lines). Furthermore the hazards that we mapped are shown as blue triangles. You can find the .csv of the hazard list in results/.*



## 1. Motivation

The severe rainfall in the central Marche region in September 2022 led to flash floods of several small rivers, which further downstream flooded small towns and villages. Due to the flooding and related landslides the transportation network was affected because of destroyed roads and bridges.

The aim of the field trip was to survey the  long term damages to the road networks which have not beend fixed more than six month after the event occured. 
Long term damages to the road network has a negative impact on the quality of life on daily basis, e.g., longer commutes to work or shopping, unaccessible fields for farmers etc.
Furthermore,  a lower accessasbiliy for emergency services may increase the rescue time and pose health hazards to the local population.

Coordinates of damages to the road infrastructure were recorded using open source software (OpenStreetMaps). Furthermore information about damages was uploaded to the OpenStreetMap database to allow for an up-to-date functionality of open source navigation and mapping services which are widely used in the scientific community.
The goals of this task force were two-fold:
1. Create a dataset which contains the damages to the road transportation network, which can be used for futher network analysis (Wassmer et al., 2022).
2. Contribute to the OpenStreetMap community by updating the information on road networks in the affected region.

## 2. Method

This section describes the method used to select points of interest (POI) for damage assessment after floods in the Italian river basins.In cooperation with several italian universities (Università di Padova, Politecnico di Milano, Università di Bolgona and based on the preliminary report on the event ,we selected the most affected river basins (Fiume Metauro, Fiume Misa, Fiume Cesano). Because of large total length of the road network in the area and time constraints a pre-selection of potential POI was necessary. For this, the roads and bridges which likely have been damaged had to be extracted. Additionally, we just surveyed major roads and did not cover streets with small relevance regarding total traffic. We selected points of interest based on the following hypothesis:

Roads which have been damaged by floods have to be in close proximity to the river network.

We therefore selected all parts of the road network which were intersecting a 65 m buffer of the river network using GIS. After visual inspection we set way poins along the functioning roads to cover all POI. To find the most efficient connection between these POI, we then applied a *travelling salesman*-algorithm to calculate the most efficient route between these points. The optimal order of the points was then transformed into a format which could be read by open route service (ORS, 2023)
While driving we tracked our own journey. Once a damaged or closed road was encountered, we generated  a „Point of Interest“ with coordinates of the location and a description of the damage. To contribute this damage to the OpenStreetMaps database we additonally created a „OSM Note“ to report this update of the road network. 

The folllowing towns were most affected by the flood:
Cantiano, Sassoferrato, Senigallia, Pergola. In these towns we asked people were the damages occured and how the affected the emergency service. This way we could save time and inspect the relevant sites in more detail.

## 3. Results
Within four days we covered about 290 km of road network within in the affected area and visited four affected towns.
Although the total area of these catchments is quite large, only a few towns and roads remain still permanently closed.
We mapped:

- 16  damaged roads
- 3  closed roads
- 5 damaged bridges
- 6 or destroyed closed bridges


![](results/figs/destroyedbridge_small.jpeg)   
*Figure 3: Destroyed bridge.*

![](results/figs/damagedroad_small.jpeg)   
*Figure 4: Damaged road.*

In most places the river is left in its natural state surrounded by floodplains or it runs in deep canyons (Figure 5).
Bridges and houses are usually built quite high above the riverbed and  the bridges contain just one arch (Figure 6). This may have led to a small rate of destroyed houses and bridges compared e.g, to the destruction in the Ahrtal 2021. Most bridges are still intact or already repaired.
The damaged houses that we saw were always build below the (ancient) city walls and close to the river (Figure 11). In the towns Cantiano and Sassoferrato, were the flooding was most severe, the river is artificially channeled and various streams flow together in the city center Figures 9, 10). In Cantiano the very narrow concrete channels seem to have spilled over. The ancient stone roads (amplfied by a lack of vegetation) then channeled the water into the city center below and inundated shops and houses up to estimated height of 1.5-2m.
Similar to the event in the Ahrtal woody debris seems to have played a big role in the generation of the flood wave. The floodwave unrooted large trees and the floating debris clogged the bridges. 
According to one farmer the previous drought had already damaged the alluvial vegetation and riverbeds were left in their natural state and not maintained. The already weakened vegetation was then carried away by the floodwave and contributed to the clogging effect (Figure 6-8).

![](results/figs/canyon_small.jpeg)   
*Figure 5: Riverbed in deep canyon.*

![](results/figs/bridge_small.jpeg)   
*Figure 6: Bridge with one arch which remained intact. On the banks of the riverbed heavy erosion of soil and vegetation, as well as unrooted trees are visible.*

![](results/figs/erosion_small.jpeg)   
*Figure 7: Heavy erorsion of the riverbanks.*

![](results/figs/tree_small.jpeg)   
*Figure 8: Unrooted trees causing damages.*

![](results/figs/cantiano1__small.jpeg)   
*Figure 9: Artifical channel in the city of Cantiano. Spilling over of these channels flooded the whole city center.*

![](results/figs/cantiano2__small.jpeg)   
*Figure 10: Artifical channel in the city of Cantiano. Spilling over of these channels flooded the whole city center.*

![](results/figs/ancientcity_small.jpeg)   
*Figure 11: In Pergola damages occured only below the ancient city walls. Houses which were build close to the river and the (clogged) bridge were heavily damaged.*


From our personal stand point, we found it interesting that several times we heard supsicions from the local population that the dramatic rainfall was „unnatural“ and caused by some unnamed authority. E.g., that the rainfall was artifically caused by planes that ejected ions into the atmosphere previous to the rainfall. We never learned what the motivation behind such a procedure might have been because we did not engage deeper into this topic, which seemingly touches conspiration theories, because of our limited skills of the italian language and a lack of time.
Despite the fact that a lot of damages were already repaired we heard many locals complain that repairs were „sloppy“ or not happening at all. An affected farmer told us that he has to repair the damaged bridges on his property to regain access to his fields but that the bureaucry took to long to realize a quick repair. He also blamed the government for not maintaing the riverbeds.

## 4. Discussion and comments:
Our mapping of the damages could have been more efficient if we:
1. Would have had a cooperation with the local road authority.
2. If we had included an DEM into our GIS analysis:
	There were many places which were, although in close proximity to the river, located high 
	above the riverbed. These points could have been excluded in our planning of the route.
3. Using a drone could have helped to get a larger overview but might have been to time costly.

Because of the partial bad condition of the roads in the central Marche region it might be advisable to use a car suited for off road driving and dirt roads.


OpenRouteService, www.openrouteservice.org, last accessed 8 May 2023.

Sandroni, P., Boccanera, F.,  Iocca, F.,  Lazzeri, M., Sofia, S. , Giordano, V.,  Sini, F.,  Speranza, G.,  Tedeschini. M. „RAPPORTO DI EVENTO preliminare, Maltempo 15, 16 e 17 settembre 2022“. Direzione Protezione Civile e Sicurezza del Territorio, Centro Funzionale Regionale. 2022

Wassmer, Jonas, Norbert Marwan, and Bruno Merz. "Impact of extreme events on topological robustness of infrastructure networks." EGU General Assembly Conference Abstracts. 2022.




