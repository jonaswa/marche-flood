#%%
import numpy as np
#from ipywidgets.embed import embed_minimal_html
import geojson
import requests

from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

import openrouteservice as ors

import geopandas as gpd

ors_key = '5b3ce3597851110001cf6248cb17b33acf834f4491e37e460a61ced2'
orsclient = ors.Client(key=ors_key)



# %%
"""Simple Travelling Salesperson Problem (TSP) between cities."""


def get_geocode(location):
    # Make a GET request to the Nominatim API
    response = requests.get('https://nominatim.openstreetmap.org/search?q={}&format=json'.format(location))

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the JSON response
        data = response.json()

        # Check if any results were found
        if len(data) > 0:
            # Get the first result
            result = data[0]

            # Extract the latitude and longitude
            latitude = result['lat']
            longitude = result['lon']

            # Return the geocode as a tuple
            return (float(latitude), float(longitude))

    # If the request failed or no results were found, return None
    return None


def create_data_model(start, end):
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = matrix
    data['num_vehicles'] = 1
    data['starts'] = [start]
    data['ends'] = [end]

    return data


def get_routes(manager, routing, solution):
    """Get vehicle routes from a solution and store them in an array."""
    print('Objective: {} meters'.format(solution.ObjectiveValue()))
    index = routing.Start(0)
    plan_output = 'Route for vehicle 0:\n'
    route_distance = 0
    route = [manager.IndexToNode(index)]
    while not routing.IsEnd(index):
        plan_output += ' {} ->'.format(manager.IndexToNode(index))
        previous_index = index
        index = solution.Value(routing.NextVar(index))
        route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
        route.append(manager.IndexToNode(index))

    plan_output += ' {}\n'.format(manager.IndexToNode(index))
    print(plan_output)
    plan_output += 'Route distance: {}meters\n'.format(route_distance)
    return route



def main(start, end):
    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model(start, end)

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['starts'], data['ends'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)


    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if solution:
        routes = get_routes(manager, routing, solution)
        return routes



#%%
start = get_geocode('Bologna, Italy')#(44.5019, 11.3331) #bologna
end = get_geocode('Bologna, Italy')#(43.6082, 13.5228) #ancona

path_to_file = 'shapes/route-points/route-day1.gpkg'
#%%

if __name__ == '__main__':
    tour_geoms = gpd.read_file(path_to_file).to_crs('epsg:4326')['geometry']
    points = list(zip(tour_geoms.geometry.x, tour_geoms.geometry.y))
    points.insert(0, start[::-1])
    points.append(end[::-1])

    matrix = orsclient.distance_matrix(
        locations=points,
        profile="driving-car",
        #metrics=['duration'],
        metrics=['duration', 'distance'],
        #validate=False,
        units = 'm'
    )

    matrix = np.ceil(matrix['distances'])
    matrix = matrix.astype(int)



    start = 0
    end = len(points)-1
    route_idxs = main(start, end)



#%%

sorted_points = [ points[p] for p in route_idxs ]
sorted_points

#%%
gmaps_url = "https://www.google.com/maps/dir/"
print(f"Open the following URL(s) on your phone to see the route in Google Maps:")
k=0
for point in sorted_points:
    gmaps_url += f"{point[1]},{point[0]}/"
    k+=1
    if k==15:
        k=0
        print(gmaps_url)
        gmaps_url = "https://www.google.com/maps/dir/"
        gmaps_url += f"{point[1]},{point[0]}/"
print(gmaps_url)





# %%
routes = orsclient.directions(sorted_points,
            profile='driving-car',
            #optimize_waypoints=True,
            format='geojson'
)

#%%
routes
# %%
with open('route-day1.geojson', 'w') as outfile:
     geojson.dump(routes, outfile)
# %%
import geopandas as gpd
gdf = gpd.read_file('route-day1.geojson')
gdf.to_file('route-day1.gpx', 'GPX', GPX_USE_EXTENSIONS='YES')
# %%

# %%
